import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:sweetapp/detail.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 16.0, top: 8),
          child: CircleAvatar(
            backgroundColor:Color(0xFFF6F7F9),
            radius: 20,
            child: IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(Icons.arrow_back_ios, size: 18,),
              color: Color(0xFF211839),
              onPressed: () {},
            ),
          ),
        ),
        title: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Container(
                  height: 45,
                  child: TextFormField(
                    cursorColor: Color(0xFFD3D4D6),
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(16),
                        borderSide: BorderSide(color: Colors.white)
                      ),
                      filled: true,
                      focusColor: Color(0xFFF6F7F9),
                      fillColor: Color(0xFFF6F7F9),
                      hintText: "Search",
                      hintStyle: TextStyle(fontSize: 14, color: Color(0xFFD3D4D6)),
                      contentPadding: EdgeInsets.all(16.0),
                    ),
                  ),
                ),
        ),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0),
                  child: CircleAvatar(
                    backgroundColor:Color(0xFFF6F7F9),
                    radius: 20,
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      icon: Icon(Icons.menu, size: 18,),
                      color: Color(0xFF211839),
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 15.0),
          CarouselSlider(
            items: <Widget>[
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Detail()));
                },
                              child: Container(
                  margin: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    boxShadow: [ 
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        blurRadius: 5,
                        offset: Offset(0, 0.5),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                      image: AssetImage('assets/doughnut.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  boxShadow: [ 
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      blurRadius: 5,
                      offset: Offset(0, 0.5),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage('assets/sweets.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  boxShadow: [ 
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      blurRadius: 5,
                      offset: Offset(0, 0.5),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage('assets/fruits.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              )
            ],
            options: CarouselOptions(
            height: 180.0,
            enlargeCenterPage: true,
            autoPlay: true,
            aspectRatio: 16 / 9,
            autoPlayCurve: Curves.fastOutSlowIn,
            enableInfiniteScroll: true,
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            viewportFraction: 0.8,
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Trending', style: TextStyle(fontSize: 16)),
              SizedBox(height: 16),
              Row(
                children: <Widget>[
                  Container(
                    height: 120,
                    width: 99,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      boxShadow: [ 
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          blurRadius: 5,
                          offset: Offset(0, 0.5),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(16.0),
                      image: DecorationImage(
                        image: AssetImage('assets/vegetables.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 95.0),
                      child: Text('Vegetables', style: TextStyle(fontSize: 12),textAlign: TextAlign.center,),
                    ),
                  ),
                  Container(
                    height: 120,
                    width: 100,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      boxShadow: [ 
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          blurRadius: 5,
                          offset: Offset(0, 0.5),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(16.0),
                      image: DecorationImage(
                        image: AssetImage('assets/fruits.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 95.0),
                      child: Text('Fruits', style: TextStyle(fontSize: 12),textAlign: TextAlign.center,),
                    ),
                  ),
                  Container(
                    height: 120,
                    width: 99,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      boxShadow: [ 
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          blurRadius: 5,
                          offset: Offset(0, 0.5),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(16.0),
                      image: DecorationImage(
                        image: AssetImage('assets/sweets.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 95.0),
                      child: Text('Sweets', style: TextStyle(fontSize: 12),textAlign: TextAlign.center,),
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    ),
  );
}}