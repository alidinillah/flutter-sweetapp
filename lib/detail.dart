import 'package:flutter/material.dart';

class Detail extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: <Widget>[
                Container(
                  width: 335,
                  height: 200,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      boxShadow: [ 
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          blurRadius: 5,
                          offset: Offset(0, 0.5),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(20.0),
                      image: DecorationImage(
                        image: AssetImage('assets/doughnut.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios, size: 18, color: Color(0xFF4F4F4F),),
                      onPressed: (){
                        Navigator.pop(context);
                      }
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Icon(Icons.share, size: 18, color: Color(0xFF4F4F4F))
                    ),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ButtonTheme(
              minWidth: 20,
              height: 30,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFFECEAF4),
                onPressed: (){},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)
                ),
                child: 
                    Text('Sweet', style: TextStyle(fontSize: 16, color: Color(0xFFBDAAD9))),
              ),
            )
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Doooonut', style: TextStyle(fontSize: 28, color: Color(0xFF4F4F4F), fontWeight: FontWeight.bold)),
                SizedBox(height: 16),
                Text('Preheat oven to 350 F, Coat a 12-to-15-cup Bundt with baking spray.',
                  style: TextStyle(color: Colors.grey), textAlign: TextAlign.justify,),
                SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Nutritional',style: TextStyle(color: Color(0xFFD78578)),),
                    SizedBox(width: 4),
                    Icon(Icons.arrow_forward_ios, size: 16, color: Color(0xFFD78578),)
                      ],
                    ),
                    CircleAvatar(
                      backgroundColor:Color(0xFFFF9EAE8),
                      radius: 20,
                      child: IconButton(
                        padding: EdgeInsets.zero,
                        icon: Icon(Icons.star, size: 16, color: Color(0xFFDC8982)),
                        color: Color(0xFF211839),
                        onPressed: () {},
                      ),
                    ),
                  ],
                )
              ]
            ),
          ),
          SizedBox(height: 130),
          ButtonTheme(
              minWidth: 328,
              height: 80,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFFF6F7F9),
                onPressed: (){},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20))
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor:Color(0xFFDC8982),
                          radius: 20,
                          child: Text(
                            'S', style: TextStyle(
                            color: Colors.white)
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:Color(0xFFF6F7F9),
                          radius: 20,
                          child: Text(
                            'M', style: TextStyle(
                            color: Color(0xFF4F4F4F))
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:Color(0xFFF6F7F9),
                          radius: 20,
                          child: Text(
                            'L', style: TextStyle(
                            color: Color(0xFF4F4F4F))
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor:Colors.white,
                          radius: 20,
                          child: Text(
                            '-', style: TextStyle(
                            color: Color(0xFF4F4F4F))
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:Color(0xFFF6F7F9),
                          radius: 20,
                          child: Text(
                            '01', style: TextStyle(
                            color: Color(0xFF4F4F4F))
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor:Colors.white,
                          radius: 20,
                          child: Text(
                            '+', style: TextStyle(
                            color: Color(0xFF4F4F4F))
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
            child: ButtonTheme(
              minWidth: 328,
              height: 70,
              child: RaisedButton(
                elevation: 0,
                color: Color(0xFF372852),
                onPressed: (){},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.shopping_basket, color: Colors.white,),
                    SizedBox(width: 16),
                    Text('Add to bag', style: TextStyle(fontSize: 16, color: Colors.white)),
                  ],
                ),
              ),
            )
      ));}}