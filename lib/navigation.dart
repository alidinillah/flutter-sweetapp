import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sweetapp/carts.dart';
import 'package:sweetapp/home.dart';
import 'package:sweetapp/profile.dart';
import 'package:sweetapp/notif.dart';

class Navigation extends StatefulWidget {
  final int page;
  Navigation(this.page);
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {

  int _currentIndex = 0;
  @override
  void initState() {
    super.initState();
    _currentIndex = widget.page;
  }
  final List<Widget> _pages = [
      Home(),
      Carts(),
      Notif(),
      Profile(),
  ];

  void onTapped(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapped,
        currentIndex: _currentIndex,
        selectedItemColor: Color(0xFF68C93E),
        items: 
      [
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/home.svg'),
          activeIcon: SvgPicture.asset('assets/home.svg', color: Color(0xFF68C93E)),
          title: Text('')
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/shopping-cart.svg'),
          activeIcon: SvgPicture.asset('assets/shopping-cart.svg', color: Color(0xFF68C93E)),
          title: Text('')
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/list.svg'),
          activeIcon: SvgPicture.asset('assets/list.svg', color: Color(0xFF68C93E)),
          title: Text('')
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset('assets/user.svg'),
          activeIcon: SvgPicture.asset('assets/user.svg', color: Color(0xFF68C93E)),
          title: Text('')
        ),
      ]
      ),
    );
  }
}