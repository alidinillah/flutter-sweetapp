import 'package:flutter/material.dart';
import 'package:sweetapp/home.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xFF362751),
        child: Column(
          children: <Widget>[
            SizedBox(height: 90),
            Text('Sweets', style: TextStyle(color: Colors.white, fontSize: 16)),
            SizedBox(height: 16),
            Image.asset('assets/welcome.png'),
            SizedBox(height: 16),
            Text('Sweet App', style: TextStyle(color: Colors.white, fontSize: 32)),
            SizedBox(height: 8),
            Text('Made by Adila Lidinillah for Sweet Lovers', style: TextStyle(color: Color(0xFF746A88), fontSize: 12)),
            SizedBox(height: 36),
            FlatButton(
              onPressed: (){}, 
              child: Text('Skip', style: TextStyle(color: Color(0xFF746A88), fontSize: 12))
            ),
            ButtonTheme(
              minWidth: 328,
              height: 48,
              child: RaisedButton(
                color: Color(0xFFDC8982),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: Text('Get Started', style: TextStyle(color: Colors.white, fontSize: 12)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}